import { TestBed } from '@angular/core/testing';

import { GscService } from '../gsc/gsc.service';

describe('GscService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GscService = TestBed.get(GscService);
    expect(service).toBeTruthy();
  });
});
