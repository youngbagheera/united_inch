import { Injectable } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class GscService {
  mainOrderList: any = [];
  mainWindowType: string;

  operatorId: number;
  mainWindowId: number;
  sizingId: number;




  constructor(public navCtrl: NavController, 
              public alertCtrl: AlertController) {
      

   }
  /**
   */
  selectTypesOfWindow(){
    this.alertCtrl.create({
      inputs: [
        {
          type: 'radio',
          label: '11/16 CBG',
          value: 'value5'
        },
        {
          type: 'radio',
          label: 'Awning',
          value: 'value5'
        },
        {
          type: 'radio',
          label: 'Casenet',
          value: 'value4'
        },
        {
          type: 'radio',
          label: 'Double Hung',
          value: 'value2'
        },
        {
          type: 'radio',
          label: 'Equal Divided Lites',
          value: 'value5'
        },
        {
          type: 'radio',
          label: 'Everwood Pine Interior',
          value: 'value5'
        },
        {
          type: 'radio',
          label: 'Glider',
          value: 'value3'
        }, 
        {
          type: 'radio',
          label: 'GliderXOX',
          value: 'value5'
        },
        {
          type: 'radio',
          label: 'Obscured',
          value: 'value5'
        },
        
        {
          type: 'radio',
          label: 'Picture',
          value: 'value5'
        },
        {
          type: 'radio',
          label: 'Tempered',
          value: 'value5'
        },
        {
          type: 'radio',
          label: 'Window Short Hand',
          value: 'value1',
          checked: true
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel')
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            console.log("radio selection data", data);
          }
        }
      ]
    }).then(alert => {
      alert.present();
    })
  }

  getUnitedInch(height: number, width: number) :number{
    return height + width;
  }


}
