
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
@Injectable()
export class DatabaseService {
    
  // we need to declare a var that will point to the initialized db:
  public db: SQLiteObject;
  
  productTable:string = "orderitemtable";
  products:any = [];



  constructor( private sqlite: SQLite){
                    
  }
  
  initalizeSqlite(){
    console.log("sqlite provider constructor called");
    this.sqlite.create({
        name: 'unitedInch.db',
        location: 'default'
      }).then((db: SQLiteObject) => {
          // here we will assign created db to our var db (see above)
          this.db = db;
          this.createorderitemTable();

          
        })
        .catch(e => console.log("SQLITE db failed to be constructed : ", e));
  }
  createorderitemTable(){
              // we can now create the table this way:
              this.db.executeSql('CREATE TABLE IF NOT EXISTS '+ this.productTable + ' (rowid INTEGER PRIMARY KEY, '+
              'qouteID TEXT,'+
              'location TEXT,'+
              'operation TEXT,'+
              'height INTEGER,'+
              'width INTEGER,'+
              'unitedInch INTEGER'
              , [])
              .then(() => {
                  console.log("loading items!!");
                  this.getSaveditems();
              })
               .catch(e => console.log("SQLite failed to create table ", e))
  }

  getSaveditems(){
    return new Promise((resolve, reject) =>{
      //Checks that the database hasn't performed any extra unneccessary queries
      this.products.length = 0;
       if(this.products.length === 0){
        let searchQuery = 'SELECT * FROM ' +this.productTable  + ' ORDER BY addedAt DESC';
        this.db.executeSql(searchQuery, [])
        .then((data) => {
            for(let i = 0; i < data.rows.length; ++i){

                let itemObj:any  = {qouteID: data.rows.item(i).qouteID,
                                    location: data.rows.item(i).location,
                                    operation: data.rows.item(i).operation,
                                    height: data.rows.item(i).height,
                                    width: data.rows.item(i).width,
                                    unitedInch: data.rows.item(i).unitedInch};
               
                this.products.push(itemObj);
            }
            
            resolve(this.products);
            console.log("done loading items!!");
        }).catch((err)=>{ console.log("error getting saved item: ",err); }) // we deal with errors etc
      } else resolve(this.products);
    });
  }
  
  addItem(item){
        //check its not a duplicate entry
              this.db.executeSql('INSERT INTO '+this.productTable + 
                             ' VALUES(NULL,?,?,?,?,?,?)', 
                              [item.qouteID,
                                item.location,
                                item.operation,
                                item.height,
                                item.width,
                                item.unitedInch
                               ]).then(value => {
                                   console.log("add item called responsed well~");
                                   this.products.push(item);
                               }).catch((err)=>{ console.log("error with adding item: ", err);});
        
  }
  addArrayOfitems(items){
    let insertRows = [];
    let newitems = [];
    for(let i = 0; i < items.length; ++i){
        insertRows.push(['INSERT INTO '+this.productTable + ' VALUES(NULL,?,?,?,?,?,?,?,?,?,?)', 
                          [ items[i].qouteID,
                            items[i].location,
                            items[i].operation,
                            items[i].height,
                            items[i].width,
                            items[i].unitedInch
                          ]
                        ]);
        newitems.push(items[i]);
    }
    this.db.sqlBatch(insertRows).then(() => {
      this.products.concat(newitems);
    }).catch(err => console.log("err with batch insert ", err));

  }
  


}