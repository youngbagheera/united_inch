import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import {switchMap} from 'rxjs/internal/operators';
import 'rxjs/add/operator/switchMap'


export class User {
  uid: string;
  username: string = '';
  constructor(auth){
    this.uid = auth.uid;
  }
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  currentUser: User;
  constructor(public afAuth: AngularFireAuth, private db: AngularFireDatabase) {
      // this.afAuth.authState.pipe(
      //   switchMap((user) => {
      //     if (user) {
      //       return this.db.object(`users/${user.uid}`);
      //     } else {
      //       return null;
      //     }
      //   })
      // ).subscribe(user => {
      //    this.currentUser['username'] = user.username;
      // })
      this.afAuth.authState.switchMap(auth => {
        if (auth) {
          this.currentUser = new User(auth)
          return this.db.object(`/users/${auth.uid}`).valueChanges();
        } else return [];
      })
      .subscribe(user => {
          try{
            this.currentUser['username'] = user.username
          }catch(e){

          }
      })
  }
  hasUserName(){
    return this.currentUser.username ? true: false;
  }
  checkUserName( username: string): Observable<any>{
    //username = username.toLowerCase();
    let obj = this.db.list('usernames/', ref => ref.orderByChild(`username`)
                                                   .equalTo(`${username}`))
                                                   .valueChanges();
    return obj;
  }
  setUserDataInFB(username: string, email: string, userName: string){
    let data = {username, key: this.currentUser.uid};
    console.log("currentUser obj", username);

    //This branch may contain other useful user meta data, last song played, friends etc
    this.db.object(`/users/${userName}`).update({username, email});

    //this branch holds all user names and is used in the checkUserName method, 
    //only the following two pieces of info should be contained in this branch to reduce data being transfered
    //while checking if the username already exist or not
    this.db.list('/usernames').push(data);
  }
  loginUser(Email: string, Password: string): Promise<any>{
    return this.afAuth.auth.signInWithEmailAndPassword(Email, Password);
  }
  async createUser(newEmail: string, newPassword: string, userName: string){
    console.log("Create user method called");
    return await this.afAuth.auth.createUserWithEmailAndPassword(newEmail, newPassword).then((result) => {
      //have to force the type to any to prevent typescript compiler error
        this.setUserDataInFB(userName, newEmail, userName);
        return true;
    }).catch(failure => {
      return false;
    });
  }
  logOut(): Promise<void>{
    return this.afAuth.auth.signOut();
  }
  resetPassword(Email: string): Promise<any>{
    return this.afAuth.auth.sendPasswordResetEmail(Email);
  }
}
