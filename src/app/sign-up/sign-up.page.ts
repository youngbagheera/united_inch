
import { EmailValidator } from './../login/email';
import { AuthService } from './../service/auth/auth.service';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { Component } from '@angular/core';

import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage  {
  signUpForm: FormGroup;
  uniqueName: string = '';
  available: boolean = false;
  firebaseObject: any;
  textInput = new FormControl('');

  KEY_FOR_EMAIL = "UserEmail";
  KEY_FOR_USERNAME = "UserName";
  
  KEY_FOR_LOGIN_STATUS = "LoginStatus";

  keyboardOn = false;

  constructor(public formBuilder: FormBuilder, 
              private authService: AuthService,
              private router: Router) {       
                
   window.addEventListener('keyboardWillHide', () => {
    // Describe your logic which will be run each time when keyboard is about to be closed.
    this.keyboardOn = false;
    });
    this.available = false;
    this.signUpForm = formBuilder.group({ email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
                                          password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    })
    this.textInput.valueChanges.subscribe(value => {
                              this.uniqueName = value;
                              this.checkUserName();
                            });

  }
  checkUserName(){
    this.authService.checkUserName(this.uniqueName).subscribe(username => {
      console.log(username);
      if(username === null || username === undefined || username.length === 0) this.available = true;
      else this.available = false;
    })
  }
  ibSignUpUser(){
    if(this.signUpForm.valid 
       && this.available 
       && this.uniqueName.length >= 2){
       this.authService.createUser(this.signUpForm.value.email, this.signUpForm.value.password, this.uniqueName)
                         .then(result => {
                            //If its not true than it will return a object with an object stating the issue of the failure
                              if(result != null){
                              this.authService.setUserDataInFB(this.uniqueName, this.signUpForm.value.email, this.signUpForm.value.password);
                              this.storeUserInfoToLocalDB();
                              this.router.navigateByUrl('/select-streamer');
                              }else{
 
                            }
                           })
                           .catch(err => {
                          
                           });

      
    }
   
  }
  goBack(){
    this.router.navigateByUrl('/login');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }
  storeUserInfoToLocalDB(){
    localStorage.setItem(this.KEY_FOR_USERNAME, this.uniqueName);
    localStorage.setItem(this.KEY_FOR_EMAIL, this.signUpForm.value.email);
    localStorage.setItem(this.KEY_FOR_LOGIN_STATUS, "true");
  }
  hideButton(){
    this.keyboardOn = true;
  }
  // validUsername(fc: FormControl){
  //   let list = this.UniqueNames.indexOf(fc.value.toLowerCase());
  //   if(list !== undefined && list!== null){
  //     return({validUsername: true})
  //   }else{
  //     return null;
  //   }
  // }


}
