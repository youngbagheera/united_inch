import { Component } from '@angular/core';
import { GscService } from '../service/gsc/gsc.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {


  customerAddress: string;
  customerPhoneNumber: string;
  customerName: string;

  newItemLocation: string;
  newItemOperation: string;
  newItemHeight: number;
  newItemWidth: number;

  constructor(public gsc: GscService) {

  }
  addItem(){
    let newItem = {
      location: this.newItemLocation,
      operation: this.newItemOperation,
      height: this.newItemHeight,
      width: this.newItemWidth
    }
    this.gsc.mainOrderList.push(newItem);
  }
  showQuote(){
    this.gsc.navCtrl.navigateForward('qoute', {})
  }
  
}
