import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QoutePage } from './qoute.page';

describe('QoutePage', () => {
  let component: QoutePage;
  let fixture: ComponentFixture<QoutePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QoutePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QoutePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
