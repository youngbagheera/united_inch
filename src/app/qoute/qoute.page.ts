import { Component, OnInit } from '@angular/core';
import { GscService } from '../service/gsc/gsc.service';

@Component({
  selector: 'app-qoute',
  templateUrl: './qoute.page.html',
  styleUrls: ['./qoute.page.scss'],
})
export class QoutePage implements OnInit {
  
  constructor(public gsc: GscService) { }

  ngOnInit() {
  }

}
