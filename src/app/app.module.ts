import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { GscService } from './service/gsc/gsc.service';
import { AuthService } from './service/auth/auth.service';
import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { IonicStorageModule } from '@ionic/storage';

import { AngularFireModule } from '@angular/fire';

var firebaseConfig = {
  apiKey: "AIzaSyCwTwcXC4hSNrXUgF2DBpgSiPO_Nq-WjHE",
  authDomain: "unitedinch-e8b94.firebaseapp.com",
  databaseURL: "https://unitedinch-e8b94.firebaseio.com",
  projectId: "unitedinch-e8b94",
  storageBucket: "unitedinch-e8b94.appspot.com",
  messagingSenderId: "130082937959",
  appId: "1:130082937959:web:4bb0564318d807f1"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig), 
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    IonicStorageModule.forRoot(),
    IonicModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    AuthService,
    GscService,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
