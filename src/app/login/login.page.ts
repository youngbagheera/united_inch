import { AuthService } from './../service/auth/auth.service';
import { Component, OnInit} from '@angular/core';
import { EmailValidator } from './email';
import { AngularFireDatabase } from 'angularfire2/database';
import { LoadingController, ToastController } from '@ionic/angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit{
  loggedIn: boolean = false;
  user: any;

  public loginForm: FormGroup;
  public loading:any;

  invalidE:boolean;
  invalidP:boolean;

  notInputing: boolean;

  previousEmail: string;

  KEY_FOR_EMAIL = "UserEmail";
  KEY_FOR_USERNAME = "UserName";
  
  KEY_FOR_LOGIN_STATUS = "LoginStatus";

  constructor(public router: Router,
             private afAuth: AngularFireAuth,
             private angularDB: AngularFireDatabase,
             public formBuilder: FormBuilder,
             public authProvider: AuthService,
             public loadingCtrl: LoadingController,
             public toastCtrl: ToastController) {

  
   this.notInputing = true;

   this.previousEmail = localStorage.getItem(this.KEY_FOR_EMAIL);

   //If the user ever navigates back to this page it means they have logged out
   //this.logout();

 }
 ngOnInit(){
  this.loginForm = this.formBuilder.group({
    email: ['', Validators.compose([
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
    ])],
    password: ['', Validators.compose([
      Validators.minLength(5),
      Validators.required,
      Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$') 
    ])]
   });
 }
 sanitizeInputField(string, ...values){

 }
 bLogin(){
     this.authProvider.loginUser(this.loginForm.value.email, this.loginForm.value.password)
                      .then(authdata => {
                         if(this.previousEmail == this.loginForm.value.email)
                         this.router.navigateByUrl('/home');
                         else
                           this.getUserKeyAndStoreLocally(this.loginForm.value.email);
                      }, error => {
                        //  this.loading = this.loadingCtrl.create({
                        //    duration: 1000
                           
                        //  });
                        //  this.loading.present(); 
                      })
 
 }
  logout() {
    localStorage.setItem(this.KEY_FOR_LOGIN_STATUS, "");

    this.afAuth.auth.signOut();
   
    //Must also delete users information after logging out & do this before forgetting spotifyAuth
    //cordova.plugins.spotifyAuth.forget();

  }
  
  async ibPromptResetPass(){
    const alertCtrl = document.querySelector('ion-alert-controller');
    await alertCtrl.componentOnReady();
    
    
    const alert = await alertCtrl.create({
      message: "Email address",
      inputs: [
        { name: 'email',
          placeholder: 'qwerty@gmail.com' }
      ],
      buttons: [
        {  text: 'Send',
           handler: data => {
             let resultToast;
             if(EmailValidator.isValid(data.email)){
               this.authProvider.resetPassword(data.email);
               resultToast = this.toastCtrl.create({
                 message: 'Reset email link has been sent',
                 duration: 2000
               });
             }else{
               resultToast =this.toastCtrl.create({
                 message: 'reset failed',
                 duration: 2000
               });
             }
             resultToast.present();
           }
        }
      ]
    });

    return await alert.present();
  }
 isValid(control: FormControl) {
   control.valueChanges
   .subscribe(value => {
     const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(
       value
     );
     if (re) {
       this.invalidE = false;
       this.invalidP = false;
       return null;
     }else{
       this.invalidE = true;
       this.invalidP = true;

       return {invalidEmail: true};
     }
   });
 }
 bNavToSignUp(){
  this.router.navigateByUrl('/sign-up');
 }
 getUserKeyAndStoreLocally(email){
   this.angularDB.database.ref('users')
                          .orderByChild('email')
                          .equalTo(email)
                          .once('value').then(result => {
                            console.log("result from login query", result);
                            //For each is just needed to help deconstruct the json object better, it will always result in one value
                            result.forEach(thisUser=> {
                               this.user = thisUser.key;
                               localStorage.setItem(this.KEY_FOR_USERNAME ,thisUser.key);
                               localStorage.setItem(this.KEY_FOR_EMAIL, this.loginForm.value.email);
                               localStorage.setItem(this.KEY_FOR_LOGIN_STATUS, "true");
                             
                               this.router.navigate(['/home', {UserName: this.user}]);
                         
                            })
                          })
 }


}
